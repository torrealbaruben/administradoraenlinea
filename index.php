<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<title>Administrado en Línea</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="/img/template/favicon.ico" rel="shortcut icon" />
	<link href="css/main.css" rel="stylesheet" />
</head>
<body>
	<header role="banner">
		<figure>
			<img alt="" src="" title="" />
		</figure>
		<nav role="navigation">
			<ul>
				<li><a href="">Inicio</a></li>
				<li><a href="">Servicio</a></li>
				<li><a href="">Clasificado</a></li>
				<li><a href="">Real Estate</a></li>
				<li><a href="">Publicar</a></li>
				<li><a href="">Condominio en Línea</a></li>
			</ul>
		</nav>
		<aside>
			<ul>
				<li><a href="">Twitter</a></li>
				<li><a href="">Facebook</a></li>
				<li><a href="">Google Plus</a></li>
				<li><a href="">RSS</a></li>
			</ul>
		</aside>
	</header>
	<main role="main">
		<section id="sliders">
			<h1>Gestiona todas las tareas administrativas asociadas a un edificio, y compatir información con los propetarios de una manera interactiva.</h1>
			<article role="article">
				<figure>
					<img alt="" src="" title="" />
				</figure>
				<ul>
					<li>Gestión global</li>
					<li>Estado de cuenta</li>
					<li>Control de fondo</li>
					<li>Presupuesto y proveedores</li>
				</ul>
			</article>
		</section>
		<section id="ultimos-items">
			<h2 class="subtitulo">El espacio que usted busca está áquí</h2>
			<article class="item" role="article">
				<h3 class="item-titulo">Titulo de la imagen</h3>
				<p class="item-descripcion">Descripción de la imagen y/o extrato del contenido.</p>
				<div class="item-precio">Bs. 2.000.000,00</div>
				<div class="item-detalle">4 cuartos - 2 baños - 300 mts2</div>
				<figure>
					<img alt="" class="item-imagen" src="" title="" />
				</figure>
			</article>
		</section>
		<section id="ventajas">
			<h2 class="subtitulo">Orientado tanto para el administrador como los propietarios</h2>
			<article role="article">
				<h3>Administrador</h3>
				<ul>
					<li>Control de gastos e ingresos.</li>
					<li>Gestión de tesorería, estado de bancos, control de morosos.</li>
					<li>Gestión documental, integrando la documentación en mis condominios.</li>
				</ul>
			</article>
			<article role="article">
				<h3>Propietario</h3>
				<ul>
					<li>Acceder a gestión documental.</li>
					<li>Conocer la situación de las incidencias.</li>
					<li>Acceso a situación financiera de la comunidad, de esta manera se obtiene total transparencia.</li>
				</ul>
			</article>
		</section>
		<footer role="contentinfo">
			<a href="">info@administradoraenlinea.com</a>
			<ul>
				<li><a href="">Inicio</a></li>
				<li><a href="">Servicio</a></li>
				<li><a href="">Clasificado</a></li>
				<li><a href="">Real Estate</a></li>
				<li><a href="">Publicar</a></li>
				<li><a href="">Condominio en Línea</a></li>
			</ul>
			<p><strong><abbr title="Sistema de gestión de condominio">Administradora en Línea</abbr></strong> - Derechos de Autor &copy; <time datetime="2014">2014</time>.</p>
			<p>Todos los derechos reservados. Todas las marcas y logos son propiedad de sus respectivos dueños.</p>
		</footer>
	</main>
	<!--[if lt IE 9]><script media="all" src="js/html5shiv-printshiv.js"></script><![endif]-->
</body>
</html>